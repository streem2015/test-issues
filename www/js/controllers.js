angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope) {

})

.controller('HomeController', function($scope) {

})

.controller('PluginsController', function($scope) {
  var vm = this;

  vm.imgsPath = 'img/';

  vm.plugins = [
    {
      imgUrl: 'note_2.svg',
      title: 'Google Calendar'
    },
    {
      imgUrl: 'weather.svg',
      title: 'Weather'
    },
    {
      imgUrl: 'Dumbbell.svg',
      title: 'Morning Exercises'
    },
    {
      imgUrl: 'note_2.svg',
      title: 'Google Calendar'
    },
    {
      imgUrl: 'weather.svg',
      title: 'Weather'
    },
    {
      imgUrl: 'Dumbbell.svg',
      title: 'Morning Exercises'
    },
    {
      imgUrl: 'note_2.svg',
      title: 'Google Calendar'
    },
    {
      imgUrl: 'weather.svg',
      title: 'Weather'
    },
    {
      imgUrl: 'Dumbbell.svg',
      title: 'Morning Exercises'
    },
    {
      imgUrl: 'note_2.svg',
      title: 'Google Calendar'
    },
    {
      imgUrl: 'weather.svg',
      title: 'Weather'
    },
    {
      imgUrl: 'Dumbbell.svg',
      title: 'Morning Exercises'
    },
    {
      imgUrl: 'note_2.svg',
      title: 'Google Calendar'
    },
    {
      imgUrl: 'weather.svg',
      title: 'Weather'
    },
    {
      imgUrl: 'Dumbbell.svg',
      title: 'Morning Exercises'
    },
  ]
});
